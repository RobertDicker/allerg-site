package com.allerg.allergsite.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@Entity
@Table(name = "additives")
public class Additive {

    @Transient
    private final List<String> acceptedType = List.of("Acidity regulator",
            "Anticaking agent",
            "Antifoaming agent",
            "Antioxidant",
            "Bleaching agent",
            "Bulking agent",
            "Carbonating agent",
            "Carrier",
            "Colour",
            "Colour retention agent",
            "Emulsifier",
            "Emulsifying salt",
            "Firming agent",
            "Flavour enhancer",
            "Flour treatment agent",
            "Foaming agent",
            "Gelling agent",
            "Glazing agent",
            "Humectant",
            "Packaging gas",
            "Preservative",
            "Propellant",
            "Raising agent",
            "Sequestrant",
            "Stabilizer",
            "Sweetener",
            "Thickener");


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @NotBlank(message = "Name cannot be empty")
    @Column(name = "name")
    private String name;

    @Pattern(message = "Not a valid E-Number, must begin with an E", regexp = "^[E][0-9]{2,4}([abcdefghijk]|[i]{1,3})?$")
    @NotBlank(message = "E-Number cannot be empty")
    @Column(name = "e_number")
    private String eNumber;

    @Size(min = 2, max = 4, message = "Must be between 2 and 4 digits")
    @NotBlank(message = "INS number cannot be empty")
    @Column(name = "ins")
    private String insNumber;

    @Column(name = "type")
    private String type;

    @Column(name = "additional_info")
    private String additionalInfo;

    public Additive() {
    }


}
