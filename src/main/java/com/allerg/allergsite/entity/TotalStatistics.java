package com.allerg.allergsite.entity;

import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Date;

@Entity
@Getter
@Table(name = "total_counter")
public class TotalStatistics {

    @Column(name = "date")
    private Date date;

    @Column(name = "total_products")
    private Integer productTotal;

    @Column(name = "total_companies")
    private Integer companyTotal;

    @Column(name = "total_ingredients")
    private Integer ingredientTotal;

    @Column(name = "total_additives")
    private Integer additiveTotal;

    @Id
    @Column(name = "id")
    private int id;
}
