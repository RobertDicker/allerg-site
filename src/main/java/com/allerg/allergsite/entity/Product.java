package com.allerg.allergsite.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@Entity
@Table(name = "products")
public class Product {

    @Column(name = "package_size")
    Integer packageSize;


    @Column(name = "is_liquid")
    Boolean isLiquid;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @NotBlank(message = "Name cannot be left blank")
    @Column(name = "name")
    private String name;

    @Valid
    @ManyToMany(cascade = {CascadeType.PERSIST,
            CascadeType.MERGE,
            CascadeType.DETACH,
            CascadeType.REFRESH})
    @JoinTable(name = "products_ingredients",
            joinColumns = @JoinColumn(name = "product_id"),
            inverseJoinColumns = @JoinColumn(name = "ingredient_id"))
    private List<Ingredient> ingredientsList;


    @Valid
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "product")
    private NutritionInfo nutritionInfo;


    @Valid
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    @JoinTable(name = "products_additives",
            joinColumns = @JoinColumn(name = "product_id"),
            inverseJoinColumns = @JoinColumn(name = "additive_id"))
    private List<Additive> additivesList;

    @Valid
    @ManyToOne(optional = true, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    private Company company;

    @Size(min = 0, max = 300, message = "This is a short description and should  be less than 300 characters")
    @Column(name = "description")
    private String description;

    @NotBlank(message = "Barcode cannot be left blank")
    @Size(min = 10, max = 128, message = "Must be betweewn 10 and 128 digits")
    @Column(name = "barcode")
    private String barcode;

    @Transient
    @Column(name = "creation_date")
    private String creationDate;

    public Boolean isLiquid() {
        if (isLiquid == null) {
            return false;
        }
        return isLiquid;
    }


    // This is used for the one to one mapping
    public void setNutritionInfo(NutritionInfo nutritionInfo) {
        this.nutritionInfo = nutritionInfo;
        nutritionInfo.setProduct(this);
    }
}
