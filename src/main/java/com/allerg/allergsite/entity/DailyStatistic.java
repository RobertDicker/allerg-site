package com.allerg.allergsite.entity;

import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "daily_statistics")
@Getter
public class DailyStatistic {

    @Id
    private Integer id;

    @Column(name = "product_count")
    private Integer product_count;

    @Column(name = "company_count")
    private Integer company_count;

    @Column(name = "ingredient_count")
    private Integer ingredient_count;

    @Column(name = "recorded_date")
    private String date;

}
