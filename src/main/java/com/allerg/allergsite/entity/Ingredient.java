package com.allerg.allergsite.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
@Entity
@Table(name = "ingredients")
public class Ingredient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @NotEmpty(message = "Name must not be blank")
    @Size(min = 2, max = 45, message = "Must be between 2 and 45 characters")
    @Column(name = "name")
    private String name;

    @Size(min = 0, max = 450, message = "Must be shorter than 45 characters")
    @Column(name = "additional_details")
    private String additionalDetails;
}

