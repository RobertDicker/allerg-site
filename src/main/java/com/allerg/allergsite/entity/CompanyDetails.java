package com.allerg.allergsite.entity;


import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


@Data
@Entity
@Table(name = "company_details")
public class CompanyDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @URL
    @Size(max = 45, message = "Must be less than 45 characters")
    @Column(name = "website")
    private String website;


    @Pattern(message = "Not a valid Phone Number", regexp = "^[\"\"]{0}|[+]?[0-9]{8,20}$")
    @Column(name = "primary_phone")
    private String primaryPhone;

    @Pattern(message = "Not a valid Phone Number", regexp = "^[\"\"]{0}|[+]?[0-9]{8,20}$")
    @Column(name = "secondary_phone")
    private String secondaryPhone;

    @Valid
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "address_id")
    private Address address;


    @Column(name = "identification_number")
    private String identificationNumber;

    @Email(message = "Email should be valid")
    @Size(max = 45, message = "Must be less than 45 characters")
    @Column(name = "email_address")
    private String email;

    @OneToOne(mappedBy = "companyDetails", fetch = FetchType.LAZY)
    private Company company;

}
