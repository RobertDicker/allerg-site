package com.allerg.allergsite.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Data
@Entity
@Table(name="addresses")
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Size(min = 0, max = 45, message = "Too long, must be shorter than 45 chars")
    @Column(name = "line1")
    private String line1;

    @Size(min = 0, max = 45, message = "Too long, must be shorter than 45 chars")
    @Column(name = "line2")
    private String line2;

    @Size(min = 0, max = 45, message = "Too long, must be shorter than 45 chars")
    @Column(name = "line3")
    private String line3;

    @Size(min = 0, max = 45, message = "Too long, must be shorter than 45 chars")
    @Column(name = "line4")
    private String line4;

    @Size(min = 0, max = 45, message = "Too long, must be shorter than 45 chars")
    @Column(name = "city")
    private String city;

    @Size(min = 0, max = 20, message = "Too long, must be shorter than 20 chars")
    @Column(name = "state")
    private String state;

    @Size(min = 0, max = 3, message = "Cannot be greater than 3 characters")
    @Column(name = "country")
    private String country;

    @Size(max = 10, message = "Must be less than 10 digits")
    @Column(name = "postcode")
    private String postcode;

    @Size(max = 100, message = "Cannot exceed 100 chars")
    @Column(name = "other_address_details")
    private String otherAddressDetails;


    @OneToOne(mappedBy = "address", fetch = FetchType.LAZY)
    private CompanyDetails companyDetails;


}
