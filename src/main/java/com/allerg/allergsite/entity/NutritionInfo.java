package com.allerg.allergsite.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "nutrition")
public class NutritionInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Min(0)
    @Column(name = "energy")
    private Integer energy;

    @Min(0)
    @Column(name = "protein")
    private Float protein;

    @Transient
    @Column(name = "total_fat")
    private Float totalFat;

    @Min(0)
    @Column(name = "saturated_fat")
    private Float saturatedFat;

    @Min(0)
    @Column(name = "unsaturated_fat")
    private Float unsaturatedFat;

    @Min(0)
    @Column(name = "carbohydrates")
    private Float carbohydrates;

    @Min(0)
    @Column(name = "sugar")
    private Float sugar;

    @Min(0)
    @Column(name = "fibre")
    private Float fibre;

    @Min(0)
    @Column(name = "sodium")
    private Float sodium;

    @Min(0)
    @Column(name = "potassium")
    private Float potassium;

    @Min(0)
    @Max(1000)
    @NotNull(message = "Please enter a value for serving size")
    @Column(name = "serving_size")
    private Integer servingSize;

    @Min(0)
    @Column(name = "servings_per_package")
    private Integer servingsPerPackage;

    @OneToOne(cascade = CascadeType.ALL, optional = false)
    @JoinColumn(name = "id")
    @MapsId
    private Product product;

}