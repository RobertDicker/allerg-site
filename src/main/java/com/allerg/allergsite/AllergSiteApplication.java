package com.allerg.allergsite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AllergSiteApplication {

    public static void main(String[] args) {
        SpringApplication.run(AllergSiteApplication.class, args);
    }

}
