package com.allerg.allergsite.controller;

import com.allerg.allergsite.entity.DailyStatistic;
import com.allerg.allergsite.entity.TotalStatistics;
import com.allerg.allergsite.service.GraphDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Arrays;

@Controller
public class DashboardController {


    private final GraphDataService graphDataService;

    @Autowired
    public DashboardController(GraphDataService graphDataService) {
        this.graphDataService = graphDataService;
    }

    @GetMapping("/")
    public String showDashboardHome(Model model) {


        model.addAttribute("main", "home/dashboard.html");
        model.addAttribute("pageTitle", "AllerG - Home");
        model.addAttribute("graph", true);

        DailyStatistic latest = graphDataService.getLatest();
        model.addAttribute("newProductCount", latest.getProduct_count());
        model.addAttribute("newCompanyCount", latest.getCompany_count());
        model.addAttribute("newIngredientCount", latest.getIngredient_count());
        model.addAttribute("data", graphDataService.getJson());

        TotalStatistics total = graphDataService.getLatestTotals();
        model.addAttribute("totalProducts", total.getProductTotal());
        model.addAttribute("totalCompanies", total.getCompanyTotal());
        model.addAttribute("totalIngredients", total.getIngredientTotal());
        model.addAttribute("totalAdditives", total.getAdditiveTotal());

        model.addAttribute("ingredientFrequency", Arrays.asList(graphDataService.getIngredientFrequency()));

        return "layout";
    }

}
