package com.allerg.allergsite.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
public interface CrudInterface<T> {


    /**
     * Gets a list of all the items for the specified request mapping entity
     * Ideally this should return "layout" your primary layout that contains fragments
     *
     * @return the layout page
     */
    @GetMapping("/list")
    String getAllItems(Model model);


    /**
     * Displays the Create a Entity Form for the particular entity
     *
     * @param model the data model
     * @return the page you wish to display that contains your createForm
     */
    @GetMapping("/create")
    String showCreateForm(Model model);

    /**
     * Displays the Update a Entity Form for the particular entity
     *
     * @param id    is the entity mapped Id of the object (the primary key)
     * @param model the data model
     * @return the page you wish to display that contains your createForm
     */
    @PostMapping("/update")
    String showUpdateForm(@Valid @RequestParam("id") int id, Model model);

    /**
     * Saves the entity to the database
     * you should use redirect e.g "redirect:list" to prevent multiple refreshes
     *
     * @param item to save
     * @return the page to return to
     */
    @PostMapping("/save")
    String save(@Valid @ModelAttribute("data") T item, BindingResult bindingResult, Model model);


    /**
     * Deletes the entity from the database
     * you should use redirect e.g "redirect:list" to prevent multiple refreshes
     *
     * @param id to delete
     * @return the page to return to
     */
    @PostMapping("/delete")
    String delete(@RequestParam("id") int id);


}
