package com.allerg.allergsite.controller;

import com.allerg.allergsite.entity.Company;
import com.allerg.allergsite.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/company")
@PropertySource("classpath:page.properties")
public class CompanyController implements CrudInterface<Company> {

    final String TABLEPAGE;
    final String ENTITYFORM;
    final String PAGETITLE;
    private final CompanyService service;
    private final String ENTITYNAME;

    @Autowired
    public CompanyController(CompanyService service, @Value("${entity.company.name}") String entityName, @Value("${entity.company.pageTitle}") String pageTitle) {

        this.service = service;
        this.ENTITYNAME = entityName;
        this.PAGETITLE = pageTitle;
        TABLEPAGE = ENTITYNAME + "/" + ENTITYNAME + "-table";
        ENTITYFORM = ENTITYNAME + "/" + ENTITYNAME + "-form";

    }

    @Override
    public String getAllItems(Model model) {

        List<Company> items = service.findAll();

        model.addAttribute(ENTITYNAME, items);
        model.addAttribute("main", TABLEPAGE);
        model.addAttribute("pageTitle", PAGETITLE);

        return "layout";

    }

    @Override
    public String showCreateForm(Model model) {

        Company item = new Company();

        List<Company> items = service.findAll();
        model.addAttribute("allcompanies", items);
        model.addAttribute("data", item);
        model.addAttribute("main", ENTITYFORM);
        model.addAttribute("pageTitle", "New " + ENTITYNAME);

        return "layout";

    }

    @Override
    public String showUpdateForm(int id, Model model) {
        Company item = service.findItemBy(id);
        List<Company> items = service.findAll();

        model.addAttribute("allcompanies", items);
        model.addAttribute("data", item);
        model.addAttribute("main", ENTITYFORM);
        model.addAttribute("pageTitle", "Update" + ENTITYNAME + ": " + item.getName());
        return "layout";
    }

    @Override
    public String save(Company item, BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("data", item);
            model.addAttribute("main", ENTITYFORM);
            model.addAttribute("pageTitle", "Update" + ENTITYNAME + ": " + item.getName());
            return "layout";
        }


        service.saveItem(item);
        return "redirect:list";
    }

    @Override
    public String delete(int id) {

        service.deleteItemWithId(id);
        return "redirect:list";
    }

}
