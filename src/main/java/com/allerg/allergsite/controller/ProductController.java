package com.allerg.allergsite.controller;

import com.allerg.allergsite.entity.Company;
import com.allerg.allergsite.entity.Product;
import com.allerg.allergsite.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/products")
@PropertySource("classpath:page.properties")
public class ProductController implements CrudInterface<Product> {

    final String TABLEPAGE;
    final String ENTITYFORM;
    final String PAGETITLE;
    private final ProductService service;
    private final String ENTITYNAME;

    @Autowired
    public ProductController(ProductService service, @Value("${entity.product.name}") String entityName, @Value("${entity.product.pageTitle}") String pageTitle) {

        this.service = service;
        this.ENTITYNAME = entityName;
        this.PAGETITLE = pageTitle;
        TABLEPAGE = ENTITYNAME + "/" + ENTITYNAME + "-table";
        ENTITYFORM = ENTITYNAME + "/" + ENTITYNAME + "-form";

    }

    @Override
    public String getAllItems(Model model) {

        List<Product> items = service.findAll();

        model.addAttribute("data", items);
        model.addAttribute("main", TABLEPAGE);
        model.addAttribute("pageTitle", PAGETITLE);

        return "layout";
    }

    @GetMapping("/product/{id}")
    public String getProduct(@PathVariable("id") int id, Model model) {

        Product product = service.findItemBy(id);

        model.addAttribute("product", product);
        model.addAttribute("main", "products/product");
        model.addAttribute("pageTitle", product.getName());

        return "layout";
    }

    @Override
    public String showCreateForm(Model model) {

        Product item = new Product();
        List<Company> companies = service.getCompanies();
        model.addAttribute("companies", companies);
        model.addAttribute("ingredients", service.getIngredients());
        model.addAttribute("additives", service.getAdditives());
        model.addAttribute("data", item);
        model.addAttribute("main", ENTITYFORM);
        model.addAttribute("pageTitle", "New " + ENTITYNAME);

        return "layout";

    }

    @Override
    public String showUpdateForm(int id, Model model) {
        Product item = service.findItemBy(id);
        List<Company> companies = service.getCompanies();
        model.addAttribute("companies", companies);
        model.addAttribute("ingredients", service.getIngredients());
        model.addAttribute("additives", service.getAdditives());
        model.addAttribute("data", item);
        model.addAttribute("main", ENTITYFORM);
        model.addAttribute("pageTitle", "Update" + ENTITYNAME + ": " + item.getName());
        return "layout";
    }

    @Override
    public String save(Product item, BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("companies", service.getCompanies());
            model.addAttribute("ingredients", service.getIngredients());
            model.addAttribute("additives", service.getAdditives());
            model.addAttribute("data", item);
            model.addAttribute("main", ENTITYFORM);
            model.addAttribute("pageTitle", "Update" + ENTITYNAME + ": " + item.getName());
            return "layout";
        }

        service.saveItem(item);
        return "redirect:list";
    }

    @Override
    public String delete(int id) {

        service.deleteItemWithId(id);
        return "redirect:list";
    }

}
