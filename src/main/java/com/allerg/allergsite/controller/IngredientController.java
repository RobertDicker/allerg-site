package com.allerg.allergsite.controller;


import com.allerg.allergsite.entity.Ingredient;
import com.allerg.allergsite.service.IngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


@Controller
@RequestMapping("/ingredients")
@PropertySource("classpath:page.properties")
public class IngredientController implements CrudInterface<Ingredient> {


    final String TABLEPAGE;
    final String ENTITYFORM;
    final String PAGETITLE;
    private final IngredientService service;
    private final String ENTITYNAME;

    @Autowired
    public IngredientController(IngredientService service, @Value("${entity.ingredient.name}") String entityName, @Value("${entity.ingredient.pageTitle}") String pageTitle) {

        this.service = service;
        this.ENTITYNAME = entityName;
        this.PAGETITLE = pageTitle;
        TABLEPAGE = ENTITYNAME + "/" + ENTITYNAME + "-table";
        ENTITYFORM = ENTITYNAME + "/" + ENTITYNAME + "-form";

    }

    @Override
    public String getAllItems(Model model) {

        List<Ingredient> items = service.findAll();

        model.addAttribute("data", items);
        model.addAttribute("main", TABLEPAGE);
        model.addAttribute("pageTitle", PAGETITLE);

        return "layout";

    }

    @Override
    public String showCreateForm(Model model) {

        Ingredient item = new Ingredient();

        model.addAttribute("data", item);
        model.addAttribute("main", ENTITYFORM);
        model.addAttribute("pageTitle", "New " + ENTITYNAME);

        return "layout";

    }

    @Override
    public String showUpdateForm(int id, Model model) {
        Ingredient item = service.findItemBy(id);

        model.addAttribute("data", item);
        model.addAttribute("main", ENTITYFORM);
        model.addAttribute("pageTitle", "Update" + ENTITYNAME + ": " + item.getName());
        return "layout";
    }

    @Override
    public String save(Ingredient item, BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("data", item);
            model.addAttribute("main", ENTITYFORM);
            model.addAttribute("pageTitle", "Update" + ENTITYNAME + ": " + item.getName());
            return "layout";
        }

        service.saveItem(item);
        return "redirect:list";
    }

    @Override
    public String delete(int id) {

        service.deleteItemWithId(id);
        return "redirect:list";
    }
}
