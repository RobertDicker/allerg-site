package com.allerg.allergsite.dao;

import com.allerg.allergsite.entity.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends GenericRepository<Product> {

    @Query("FROM Product p WHERE p.barcode = :barcode")
    Product findByBarcode(@Param("barcode") String barcode);

    @Query("FROM Product p WHERE p.name = :name")
    Product findByName(@Param("name") String name);

    @Query("Select name FROM Product")
    List<String> getAllNames();


}
