package com.allerg.allergsite.dao;

import com.allerg.allergsite.entity.Company;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(path = "companies")
public interface CompanyRepository extends GenericRepository<Company> {
}
