package com.allerg.allergsite.dao;

import com.allerg.allergsite.entity.Additive;
import org.springframework.stereotype.Repository;

@Repository
public interface AdditiveRepository extends GenericRepository<Additive> {
}
