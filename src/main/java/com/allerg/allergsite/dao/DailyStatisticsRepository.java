package com.allerg.allergsite.dao;

import com.allerg.allergsite.entity.DailyStatistic;
import com.allerg.allergsite.entity.TotalStatistics;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DailyStatisticsRepository extends GenericRepository<DailyStatistic> {


    @Query("select (d) from DailyStatistic d order by d.date desc")
    List<DailyStatistic> getAllSortedByDateDesc();


    @Query("select (d) from DailyStatistic d order by d.date")
    List<DailyStatistic> getAllSortedByDateAsc();

    @Query("SELECT (t) from TotalStatistics t")
    List<TotalStatistics> getTotals();

    @Query(value = "SELECT CONCAT(i.name, ' ',count(*)) AS VALUE from products_ingredients join ingredients i on products_ingredients.ingredient_id = i.id group by ingredient_id", nativeQuery = true)
    List<String> getIngredientFrequency();


}
