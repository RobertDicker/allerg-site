package com.allerg.allergsite.dao;

import com.allerg.allergsite.entity.Ingredient;
import org.springframework.stereotype.Repository;

@Repository
public interface IngredientRepository extends GenericRepository<Ingredient> {
}
