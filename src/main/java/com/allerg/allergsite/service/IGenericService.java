package com.allerg.allergsite.service;

import java.util.List;

public interface IGenericService<T> {

    T findItemBy(int itemId);

    List<T> findAll();

    T saveItem(T item);

    boolean deleteItemWithId(int itemId);
}
