package com.allerg.allergsite.service;

import com.allerg.allergsite.dao.IngredientRepository;
import com.allerg.allergsite.entity.Ingredient;
import org.springframework.stereotype.Service;

@Service
public class IngredientService extends AbstractGenericService<Ingredient> {

    public IngredientService(IngredientRepository repository) {
        super(repository);
    }
}

