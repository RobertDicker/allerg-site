package com.allerg.allergsite.service;

import com.allerg.allergsite.dao.AdditiveRepository;
import com.allerg.allergsite.dao.CompanyRepository;
import com.allerg.allergsite.dao.IngredientRepository;
import com.allerg.allergsite.dao.ProductRepository;
import com.allerg.allergsite.entity.Additive;
import com.allerg.allergsite.entity.Company;
import com.allerg.allergsite.entity.Ingredient;
import com.allerg.allergsite.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService extends AbstractGenericService<Product> implements IProductService {

    ProductRepository repository;
    CompanyRepository companyRepository;
    AdditiveRepository additiveRepository;
    IngredientRepository ingredientRepository;

    @Autowired
    public ProductService(ProductRepository repository, CompanyRepository companyRepository, AdditiveRepository additiveRepository, IngredientRepository ingredientRepository) {
        super(repository);
        this.companyRepository = companyRepository;
        this.repository = repository;
        this.additiveRepository = additiveRepository;
        this.ingredientRepository = ingredientRepository;
    }

    //TODO implement
    @Override
    public Product getProductByBarcode(String barcode) {

        return repository.findByBarcode(barcode);
    }

    @Override
    public Product getProductByName(String name) {
        return repository.findByName(name);
    }


    public List<Company> getCompanies() {
        return companyRepository.findAll();
    }

    public List<Ingredient> getIngredients() {
        return ingredientRepository.findAll();
    }

    public List<Additive> getAdditives() {
        return additiveRepository.findAll();
    }


}
