package com.allerg.allergsite.service;


import com.allerg.allergsite.dao.GenericRepository;

import java.util.List;
import java.util.Optional;

public abstract class AbstractGenericService<T> implements IGenericService<T> {

    private final GenericRepository<T> repository;


    public AbstractGenericService(GenericRepository<T> repository) {
        this.repository = repository;
    }

    @Override
    public T findItemBy(int productId) {
        Optional<T> result = repository.findById(productId);

        T theIngredient;

        if (result.isPresent()) {
            theIngredient = result.get();
        } else {
            throw new RuntimeException("Did not find theIngredient id - " + productId);
        }

        return theIngredient;
    }

    @Override
    public List<T> findAll() {
        return repository.findAll();
    }

    @Override
    public T saveItem(T theIngredient) {
        return repository.save(theIngredient);
    }

    @Override
    public boolean deleteItemWithId(int ingredientId) {

        Optional<T> result = repository.findById(ingredientId);

        if (result.isEmpty()) {
            return false;
        }

        repository.deleteById(ingredientId);
        return true;

    }
}
