package com.allerg.allergsite.service;

import com.allerg.allergsite.dao.CompanyRepository;
import com.allerg.allergsite.entity.Company;
import org.springframework.stereotype.Service;

@Service
public class CompanyService extends AbstractGenericService<Company> {

    public CompanyService(CompanyRepository repository) {
        super(repository);
    }
}
