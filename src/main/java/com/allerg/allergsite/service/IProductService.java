package com.allerg.allergsite.service;

import com.allerg.allergsite.entity.Product;

public interface IProductService {

    Product getProductByBarcode(String barcode);

    Product getProductByName(String name);
}
