package com.allerg.allergsite.service;

import com.allerg.allergsite.dao.DailyStatisticsRepository;
import com.allerg.allergsite.entity.DailyStatistic;
import com.allerg.allergsite.entity.TotalStatistics;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@Service
public class GraphDataService {


    DailyStatisticsRepository dailyStatisticsRepository;

    private List<DailyStatistic> statisticsCacheAsc;

    @Autowired
    public GraphDataService(DailyStatisticsRepository dailyStatisticsRepository) {
        this.dailyStatisticsRepository = dailyStatisticsRepository;
    }

    public Map<String, Integer> findAllDailyProductStatistics() {

        Map<String, Integer> map = new TreeMap<>();

        if (statisticsCacheAsc == null) {
            statisticsCacheAsc = dailyStatisticsRepository.getAllSortedByDateAsc();
        }

        for (DailyStatistic stats : statisticsCacheAsc) {

            map.put(stats.getDate(), stats.getProduct_count());

        }

        return map;
    }


    public Map<String, Integer> findAllDailyAddedCompanyStatistics() {


        Map<String, Integer> map = new TreeMap<>();

        if (statisticsCacheAsc == null) {
            statisticsCacheAsc = dailyStatisticsRepository.getAllSortedByDateAsc();
        }

        for (DailyStatistic stats : statisticsCacheAsc) {

            map.put(stats.getDate(), stats.getCompany_count());
        }

        return map;
    }

    public DailyStatistic getLatest() {
        return dailyStatisticsRepository.getAllSortedByDateDesc().get(0);
    }


    public String getJson() {
        if (statisticsCacheAsc == null) {
            statisticsCacheAsc = dailyStatisticsRepository.getAllSortedByDateAsc();
        }

        ObjectMapper objectMapper = new ObjectMapper();

        try {
            return objectMapper.writeValueAsString(statisticsCacheAsc);
        } catch (Exception e) {
            return "";
        }
    }


    public TotalStatistics getLatestTotals() {
        return dailyStatisticsRepository.getTotals().get(0);
    }


    public String getIngredientFrequency() {


        ObjectMapper objectMapper = new ObjectMapper();

        List<Object[]> list = new ArrayList<>();

        for (String s : dailyStatisticsRepository.getIngredientFrequency()) {
            Object[] pair = {s.split(" ")[0], Integer.parseInt(s.split(" ")[1])};
            list.add(pair);
        }
        try {

            return objectMapper.writeValueAsString(list);
        } catch (Exception e) {
            return "";
        }


    }
}
