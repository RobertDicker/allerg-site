package com.allerg.allergsite.service;

import com.allerg.allergsite.dao.AdditiveRepository;
import com.allerg.allergsite.entity.Additive;
import org.springframework.stereotype.Service;

@Service
public class AdditiveService extends AbstractGenericService<Additive> {
    public AdditiveService(AdditiveRepository repository) {
        super(repository);
    }
}
